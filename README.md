# Setup django graphene to use Enum
In this repo is contained the study to undestand how to correctly setup the Mutation when use Enum django. In particular we undestand that using `DjangoModelFormMutation` it put `String!` as the type and not the Enum type. To have the Enum type we must use different way that can be find in [/backend/enum_play/schema.py](/backend/enum_play/schema.py) file.

```
python3.10 -m venv venv
source venv/bin/activate.fish

pip install -r requirements.txt

./backend/manage.py migrate
./backend/manage.py createsuperuser

./backend/manage.py runserver
# go to localhost:8000/graphql
```
* query examples:
```
query {
  allSomeModels{
    id,
    enumField
  }
}


mutation MyMutation {
  updateSomeidModels(input: {enumField: B, id: "2"}) {
    someModel {
      enumField
      id
    }
  }
}
```

# References
What is the requirements to undestand that codebase?
* python
* venv
* django
* graphene-django