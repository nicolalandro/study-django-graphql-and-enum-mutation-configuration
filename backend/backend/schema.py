import graphene
from enum_play.schema import Query, Mutation

class FullQuery(graphene.ObjectType, Query):
    hello = graphene.String(default_value="Hi!")


class AllMutation(graphene.ObjectType, Mutation):
    pass

schema = graphene.Schema(query=FullQuery, mutation=AllMutation)