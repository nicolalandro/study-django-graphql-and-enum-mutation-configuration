from django.apps import AppConfig


class EnumPlayConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'enum_play'
