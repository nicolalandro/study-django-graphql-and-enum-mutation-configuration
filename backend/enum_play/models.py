from django.db import models

class SomeModel(models.Model):

    class SomeEnum(models.TextChoices):

        A = "A", ("Option A")
        B = "B", ("Option B")

    enum_field = models.CharField(
        max_length=1,
        choices=SomeEnum.choices,
        default=SomeEnum.A,
    )