from .models import SomeModel
import graphene
from graphene_django import DjangoObjectType
from graphene_django.fields import Field
from graphene_django.forms.mutation import DjangoModelFormMutation
from django.forms import ModelForm
from graphene import relay
from graphql_relay import from_global_id

SomeEnumSchema = graphene.Enum.from_enum(SomeModel.SomeEnum)

class SomeModelType(DjangoObjectType):
    class Meta:
        model = SomeModel
        fields = ("id", "enum_field")
        # convert_choices_to_enum = True
        enum_field = SomeEnumSchema(required=True)

class SomeModelForm(ModelForm):
    class Meta:
        model = SomeModel
        fields = ('enum_field',)

class SomeDjMutation(DjangoModelFormMutation):       
     
    class Meta:
        form_class = SomeModelForm

class SomeMutation(graphene.Mutation):
    class Arguments:
        some_enum = SomeEnumSchema(required=True)
        id = graphene.ID()
    some_model = graphene.Field(SomeModelType)

    @classmethod
    def mutate(cls, root, info, some_enum, id):
        sm = SomeModel.objects.get(pk=id)
        sm.enum_field = str(some_enum)
        sm.save()
        return SomeMutation(some_model=sm)

# more similar to DjangoModelFormMutation
class SomeIDMutation(relay.ClientIDMutation):
    class Input:
        enum_field = SomeEnumSchema(required=True)
        id = graphene.ID()

    some_model = graphene.Field(SomeModelType)

    @classmethod
    def mutate_and_get_payload(cls, root, info, enum_field, id):
        sm = SomeModel.objects.get(pk=id)
        sm.enum_field = str(enum_field)
        sm.save()
        return SomeMutation(some_model=sm)


class Mutation():
    update_some_models = SomeMutation.Field()
    update_somejd_models = SomeDjMutation.Field()
    update_someid_models = SomeIDMutation.Field()

class Query():
    all_some_models = graphene.List(SomeModelType)

    def resolve_all_some_models(root, info):
        return SomeModel.objects.all()

